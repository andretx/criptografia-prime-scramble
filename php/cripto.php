<?php
/**
 * Criptografia Prime-Scramble
 *
 * @package  php
 * @author   andretx
 */

if( ( !array_key_exists(1, $argv) && !array_key_exists(2, $argv) ) || ( !isset($argv[2]) || ( $argv[2] != 'des' && $argv[2] != 'enc' ) ) ) {
	echo "Por favor informe um valor\n";
	echo "Exe: php cripto.php 'string' tipo \n";
	echo "tipo: 'des' ou 'enc' - des => desencriptar, enc => encriptar \n";
	exit;
}

$CIFRA = $argv[1];
$TIPO = $argv[2];
$cifra_array = str_split($CIFRA);

if( $TIPO == 'des' )
	$tipo = 'TEXTO LIMPO';
else
	$tipo = 'TEXTO CIFRADO';

$letra_encontrada = array();

$inferior = 33;
$superior = 125;

$Primos = array();
$p = 0;
$k = 0;
while( $p <= 820 ) {
	$primo = gmp_prob_prime($p);
	if( $primo ) {
		$Primos[$k] = $p;
		$k++;
	}

	if( $k == 130 )
		break;
$p++;
}

foreach( $cifra_array as $key => $value ) {

	$asc_cifra = ord($value);
	$Pvalue = $Primos[$key];

	if( $TIPO == 'des' ) {
		$valor = $asc_cifra - $Pvalue - $inferior;
		if( $valor < 0 ) {
			$valor = abs($valor);
			$buscando = abs($valor - ( $superior + 1));
		} else {
			$buscando = $valor + $inferior;
		}
		$letra_encontrada[] = chr($buscando);
	} else if( $TIPO == 'enc' ) {
		$caracter_achado = $asc_cifra + $Pvalue;
		if( $caracter_achado > $superior ) {
			$caracter_achado = $inferior + ( $caracter_achado - $superior - 1 ) % $superior;
		}
		$letra_encontrada[] = chr($caracter_achado);
	}
}

echo "########################################\n\n";
echo " ".$tipo.": \n";
echo "\n ".implode('',$letra_encontrada)."\n\n";
echo "########################################\n";
