#!/usr/bin python
'''
Criptografia Prime-Scramble

@package  python
@author   andretx
'''

import sys

if len(sys.argv) != 3:
	print "########################################\n";
	print "	Por favor informe um valor";
	print "	Exe: python cripto.py 'string' tipo";
	print "	tipo: 'des' ou 'enc' - des => desencriptar, enc => encriptar";
	print "\n########################################";
else:

	inferior = int(33)
	superior = int(125)

	CIFRA = tuple(sys.argv[1])
	TIPO = sys.argv[2].lower()

	if TIPO == 'des':
		tipo = 'TEXTO LIMPO'
	else:
		tipo = 'TEXTO CIFRADO'

	letra_encontrada = []
	primos = []
	with open("primos.txt") as arquivo:
	    for line in arquivo:
		primos.append(line.strip())


	for index, letra in enumerate(CIFRA):

		asc_cifra = int(ord(letra))
		Pvalue = int(primos[index])

		if TIPO == 'des':
			valor = asc_cifra - Pvalue - inferior
			if valor < 0:
				valor = abs(valor)
				buscando = abs(valor - ( superior + 1))
			else:
				buscando = valor + inferior

			letra_encontrada.append(chr(buscando))
		elif TIPO == 'enc':
			caracter_achado = asc_cifra + Pvalue;
			if caracter_achado > superior:
				caracter_achado = inferior + ( caracter_achado - superior - 1 ) % superior
		
			letra_encontrada.append(chr(caracter_achado))

	print "########################################\n\n";
	print " ",tipo,": \n";
	print " ","".join(letra_encontrada),"\n\n";
	print "########################################\n\n";


