# Criptografia Prime-Scramble
### Teste criptografia - MercaFácil
O valor ASCII de cada membro da string é somado ao número primo de mesmo índice.
- - -

Linguagens usadas:

* **PHP** na versão PHP 7.2 
```php
php cripto.php 'CIFRA' des
```
```php
php cripto.php 'TEXTOCLARO' enc
```
**gmp_prob_prime** — Verifica se o número é "provavelmente primo"<br>
Para instalar php*-gmp no Ubuntu 16.04 você precisa executar os seguintes comandos no terminal:
```php
sudo apt update
sudo apt install php*-gmp
```

* **Python** na versão 2.7
```python
python cripto.py 'CIFRA' des
```
```python
python cripto.py 'TEXTOCLARO' enc
```
**dicas** gerando lista de números primos em um txt, para ser usada no script py
```bash
seq 2 800 | factor | awk -F \: '$1 == $2{ print $1}' > primos.txt
```

---

os testes foram realizados na versões acima citadas.

